﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using XInputDotNetPure;

public class PlayerController : MonoBehaviour
{


    public static List<PlayerController> Players = new List<PlayerController>();


    public Camera camera;
    public WheelCollider backWheel1;
    public WheelCollider backWheel2;
    public WheelCollider frontWheel1;
    public WheelCollider frontWheel2;

    public Transform frontWheel1Trans;
    public Transform frontWheel2Trans;

    public float MotorForce;
    public float BreakForce;

    public Light breakLight1;
    public Light breakLight2;

    private float turboManipulation = 1;
    private float airControllManipulation = 1;

    public int playerNumber = 0;

    private string playerPrefix;

    public SoldierController SoldierController;

    public int[] gearRatio;
    private int gear;

    public Rigidbody Rocket;
    public Rigidbody Bullet;
    public Rigidbody Barrel;

    public WeaponType weaponType = WeaponType.Rocket;
    private int ammo = 500;

    public Color playerColor;

    private Text speedText;
    private Text weaponText;
    private Text scoreText;

    public int maxSpeed = 200;

    public int score = 0;


    private Transform targetLookAt;
    void Start()
    {
        playerPrefix = "P" + playerNumber + "_";
        Players.Add(this);
        //SoldierController.RagDollActive = false;

        speedText = getText("speed");
        weaponText = getText("WeaponText");
        getText("PlayerText").text = "Player " + playerNumber;
        scoreText = getText("score");

        Transform carBase = transform.FindChild("carOb").FindChild("carbase");

        foreach (Transform trans in carBase)
        {
            if (trans.renderer != null)
                trans.renderer.material.color = playerColor;

        }

        targetLookAt = transform.FindChild("VisualStuff").FindChild("targetLookAt");

        rigidbody.centerOfMass = new Vector3(0, -0.5f, 0);

    }

    private Text getText(string name)
    {
        Transform trans = transform.FindChild("VisualStuff").FindChild("Canvas").FindChild("Panel").FindChild(name);
        return trans.GetComponent<Text>();
    }


    void OnDestroy()
    {
        Players.Remove(this);
    }

    // Update is called once per frame
    void Update()
    {

        //Time.timeScale = 4;

        if (playerNumber > -1)
        {


            float v = Input.GetAxis(playerPrefix + "Vertical") * MotorForce * turboManipulation;
            Debug.Log(playerNumber + " " + v);


            if (v < 0)
            {
                targetLookAt.localEulerAngles = new Vector3(0, 180, 0);
            }
            else if (v > 0)
            {
                targetLookAt.localEulerAngles = new Vector3(0, 0, 0);
            }


            float h = Input.GetAxis(playerPrefix + "Horizontal") * 25;

            frontWheel1.steerAngle = h;
            frontWheel2.steerAngle = h;

            frontWheel1Trans.localRotation = new Quaternion(90, 90, frontWheel1.steerAngle * -1, 1);
            frontWheel2Trans.localRotation = new Quaternion(90, 90, frontWheel1.steerAngle * -1, 1);


            keyEvents();

            if (Speed <= maxSpeed)
            {
                backWheel1.motorTorque = v;
                backWheel2.motorTorque = v;

                //Allrad
                frontWheel1.motorTorque = v;
                frontWheel2.motorTorque = v;
            }
            else
            {
                backWheel1.motorTorque = 0;
                backWheel2.motorTorque = 0;

                //Allrad
                frontWheel1.motorTorque = 0;
                frontWheel2.motorTorque = 0;
            }


        }



        if (!frontWheel1.isGrounded && !frontWheel2.isGrounded && !backWheel1.isGrounded && !backWheel2.isGrounded)
        {
            //wait 2 sec than show Message to reset
            //SoldierController.RagDollActive = true;

        }
        else
        {
            //SoldierController.RagDollActive = false;
        }
        Vector3 eulerRotation = transform.rotation.eulerAngles;
        if (eulerRotation.x > 90 || eulerRotation.x < -90 || eulerRotation.y > 90 || eulerRotation.y < -90 || eulerRotation.z > 90 || eulerRotation.z < -90)
        {

        }
        EngineSound();

    }

    private void keyEvents()
    {

        if (playerNumber > 0)
        {

            if (Input.GetKey("joystick " + playerNumber + " button 2"))
            {
                backWheel1.brakeTorque = BreakForce;
                backWheel2.brakeTorque = BreakForce;
                //frontWheel1.brakeTorque = BreakForce;
                //frontWheel2.brakeTorque = BreakForce;
                breakLight1.intensity = 2.5f;
                breakLight2.intensity = 2.5f;
            }
            if (Input.GetKeyUp("joystick " + playerNumber + " button 2"))
            {
                backWheel1.brakeTorque = 0;
                backWheel2.brakeTorque = 0;
                //frontWheel1.brakeTorque = 0;
                //frontWheel2.brakeTorque = 0;
                breakLight1.intensity = 0.4f;
                breakLight2.intensity = 0.4f;
            }

            if (Input.GetKeyDown("joystick " + playerNumber + " button 1"))
            {
                Reset();

            }
            if (Input.GetKeyDown("joystick " + playerNumber + " button 6"))
            {
                Reset();
                transform.position = new Vector3(0, 500, 0);

            }



            if (Input.GetKeyDown("joystick " + playerNumber + " button 5"))
            {
                Shoot();
            }




            if (Input.GetKeyDown("joystick " + playerNumber + " button 0"))
            {
                turboManipulation = 3;
            }
            if (Input.GetKeyUp("joystick " + playerNumber + " button 0"))
            {
                turboManipulation = 1;
            }


            if (Input.GetKey("joystick " + playerNumber + " button 4"))
            {
                StartCoroutine(ShootBullet());
            }

            if (Input.GetKeyDown("joystick " + playerNumber + " button 0"))
            {
                turboManipulation = 3;
            }
            if (Input.GetKeyUp("joystick " + playerNumber + " button 0"))
            {
                turboManipulation = 1;
            }


        }
        else
        {
            if (Input.GetKey(KeyCode.Space))
            {
                backWheel1.brakeTorque = BreakForce;
                backWheel2.brakeTorque = BreakForce;
                //frontWheel1.brakeTorque = BreakForce;
                //frontWheel2.brakeTorque = BreakForce;
                breakLight1.intensity = 2.5f;
                breakLight2.intensity = 2.5f;
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                backWheel1.brakeTorque = 0;
                backWheel2.brakeTorque = 0;
                //frontWheel1.brakeTorque = 0;
                //frontWheel2.brakeTorque = 0;
                breakLight1.intensity = 0.4f;
                breakLight2.intensity = 0.4f;
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                Reset();

            }
            if (Input.GetKeyDown(KeyCode.T))
            {
                Reset();
                transform.position = new Vector3(0, 500, 0);

            }



            if (Input.GetKeyDown(KeyCode.K))
            {
                Shoot();
            }
            if (Input.GetKey(KeyCode.J))
            {
                StartCoroutine(ShootBullet());
            }

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                turboManipulation = 3;
            }
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                turboManipulation = 1;
            }


        }


    }


    private void EngineSound()
    {
        //audio.pitch = Speed / 200 + 0.1f;

        for (int i = 0; i < gearRatio.Length; i++)
        {
            if (gearRatio[i] > Speed)
            {
                gear = i;
                break;
            }
        }
        float gearMinValue = 0.00f;
        float gearMaxValue = 0.00f;
        if (gear == 0)
        {
            gearMinValue = 0f;
        }
        else
        {
            gearMinValue = gearRatio[gear - 1];
        }
        gearMaxValue = gearRatio[gear];



        float enginePitch = ((Speed - gearMinValue) / (gearMaxValue - gearMinValue)) + 1;
        audio.pitch = enginePitch * turboManipulation; //was currentSpeed / topSpeed + 1;


    }


    IEnumerator VibrateForMillis()
    {
        Debug.Log(playerNumber);
        GamePad.SetVibration(PlayerIndex.One, 0.5f, 0.5f);


        yield return new WaitForSeconds(0.2f);
        GamePad.SetVibration(PlayerIndex.One, 0, 0);
        Debug.Log(playerNumber);
    }


    private void ThrowOutBody()
    {
        SoldierController.RagDollActive = true;
    }

    private void Reset()
    {
        backWheel1.motorTorque = 0;
        backWheel2.motorTorque = 0;
        backWheel1.brakeTorque = 0;
        backWheel2.brakeTorque = 0;
        transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, 0));
        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;



    }


    private void Shoot()
    {
        //StartCoroutine(VibrateForMillis());

        if (ammo > 0)
        {

            switch (weaponType)
            {
                case WeaponType.Rocket:
                    {
                        Rigidbody clone = (Rigidbody)Instantiate(Rocket, transform.position + transform.forward * 10, transform.rotation);
                        clone.GetComponent<Rocket>().player = this;
                        clone.velocity = transform.TransformDirection(Vector3.forward * 100) + rigidbody.velocity;

                        break;
                    }
                case WeaponType.Barrel:
                    {
                        Rigidbody clone = (Rigidbody)Instantiate(Barrel, transform.position - transform.forward * 10, transform.rotation);


                        break;
                    }


            }
            ammo--;
        }

    }

    private bool shootBulletAllow = true;

    private IEnumerator ShootBullet()
    {
        if (shootBulletAllow)
        {
            shootBulletAllow = false;
            Rigidbody clone; //variable within a variable

            clone = (Rigidbody)Instantiate(Bullet, transform.position + transform.forward * 5, transform.rotation); //the clone variable holds our instantiate action
            clone.GetComponent<Bullet>().player = this;

            clone.velocity = transform.TransformDirection(Vector3.forward * 200 + new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), Random.Range(-2, 2))) + rigidbody.velocity; //applies force to our pr
            yield return new WaitForSeconds(0.1f);
            shootBulletAllow = true;
        }
    }



    public float Speed = 0;
    Vector3 lastPosition = Vector3.zero;

    void FixedUpdate()
    {

        Speed = (transform.position - lastPosition).magnitude / Time.deltaTime * 3.6f / 2;
        camera.fieldOfView = 60 + Speed / 8;
        lastPosition = transform.position;
        speedText.text = (int)Speed + " kph";
        weaponText.text = weaponType + " " + ammo;
        scoreText.text = "Score: " + score;



        if (!frontWheel1.isGrounded && !frontWheel2.isGrounded && !backWheel1.isGrounded && !backWheel2.isGrounded)
        {

            if (playerNumber > -1)
            {


                float roll = Input.GetAxis(playerPrefix + "Horizontal") * -1 * 500;
                float pitch = Input.GetAxis(playerPrefix + "Horizontal_Y") * -1 * 500;

                rigidbody.AddRelativeTorque(Vector3.right * pitch * airControllManipulation, ForceMode.Impulse);
                //rigidbody.AddRelativeTorque(Vector3.up * yaw * finalRotFactor, ForceMode.Impulse);
                rigidbody.AddRelativeTorque(Vector3.forward * roll * airControllManipulation, ForceMode.Impulse);

            }
        }

    }

    public enum WeaponType
    {
        Rocket,
        Barrel
    }

}
