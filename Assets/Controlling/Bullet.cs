﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

    public PlayerController player;

	// Use this for initialization
	void Start () {
        Destroy(gameObject, 5);
	}


    void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);

        Rigidbody rigi = collision.collider.attachedRigidbody;
        if (rigi)
        {
            PlayerController controller = rigi.GetComponent<PlayerController>();
            if (controller)
            {
                player.score += 10;
            }

        }
    }

}
