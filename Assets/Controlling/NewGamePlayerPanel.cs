﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NewGamePlayerPanel : MonoBehaviour {


    public int PlayerId = 1;
    public Image colorPreview;
    public Image startButton;
    public Image controllerTypeImage;
    public Text playerIdText;


    public bool ready = false;
    public Color color;


    public Color[] colors;
    private int actColorChoice = 0;

    public bool keyBoardAble = false;


    void Start()
    {
        Debug.Log(colors.Length);
        actColorChoice = Random.Range(0,colors.Length-1);
        color = colors[actColorChoice];
    }

	// Update is called once per frame
	void Update () {

        if (PlayerId >= 1)
        {
            controllerTypeImage.enabled = true;
            playerIdText.enabled = true;
            playerIdText.text = PlayerId.ToString();
            if (Input.GetKeyDown("joystick " + PlayerId + " button 2"))
            {
                nextColor();
            }

            if (Input.GetKeyDown("joystick " + PlayerId + " button 7"))
            {
                toogleRDY();
            }
        }
        else if (PlayerId == 0)
        {
            controllerTypeImage.enabled = false;
            playerIdText.enabled = false;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                nextColor();
            }

            if (Input.GetKeyDown(KeyCode.Return))
            {
                toogleRDY();
            }


        }


        colorPreview.color = color;

        if (ready)
        {
            startButton.color = new Color(0, 1, 0);
        }
        else
        {
            startButton.color = new Color(1, 1, 1);
        }
      

	}

    private void toogleRDY()
    {
        ready = !ready;
      
    }

    private void nextColor()
    {  //color = new  Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
        actColorChoice++;
        if (actColorChoice >= colors.Length)
        {
            actColorChoice = 0;
        }
        color = colors[actColorChoice];
     

    }


}
