﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour {

    public Transform car;
    public Text pressAnyKeyText;


    void Start()
    {
        StartCoroutine(TextBlink());
    }

	void Update () {
        car.localEulerAngles = Vector3.Lerp(car.localEulerAngles, new Vector3(2, car.localEulerAngles.y + 10, 0), Time.deltaTime * 1);
        if (Input.anyKeyDown)
        {
            Application.LoadLevel("NewGame");
        }
	}

    IEnumerator TextBlink()
    {
        yield return new WaitForSeconds(0.5f);
        pressAnyKeyText.enabled = !pressAnyKeyText.enabled;
        StartCoroutine(TextBlink());
    }

}
