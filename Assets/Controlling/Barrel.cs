﻿using UnityEngine;
using System.Collections;

public class Barrel : MonoBehaviour {

    public float radius = 2000.0f;    //provides a radius at which the explosive will effect rigidbodies
    public float power = 500000.0f;    //provides explosive power
    public float explosiveLift = 10f; //deter
    public Transform explosionParticle;

	// Use this for initialization
	void Start () {
	
	}


    void OnCollisionEnter(Collision collision)
    {

        Debug.Log(collision.collider.gameObject.layer);
        if (collision.collider.gameObject.layer != LayerMask.NameToLayer("BarrelHit"))
        {
            return;
        }

        Vector3 grenadeOrigin = transform.position;

        Collider[] colliders = Physics.OverlapSphere(grenadeOrigin, radius); //this is saying that if any collider within the radius of our object will feel the explosion

        Instantiate(explosionParticle, grenadeOrigin, transform.rotation);


        foreach (Collider hit in colliders)
        {  //for loop that says if we hit any colliders, then do the following below

            Rigidbody rigidbody = hit.rigidbody;
            if (rigidbody == null)
            {
                rigidbody = hit.attachedRigidbody;

                Debug.Log(hit + " " + rigidbody);
            }

            if (rigidbody)
            {
                Debug.Log(hit);
                rigidbody.AddExplosionForce(power, grenadeOrigin, radius, explosiveLift); //if we hit any rigidbodies then add force based off our power, the position of the explosion object

                Destroy(gameObject);    //the radius and finally the explosive lift. Afterwards destroy the game object

            }

        }

    }



}
