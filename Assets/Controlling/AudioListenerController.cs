﻿using UnityEngine;
using System.Collections;

public class AudioListenerController : MonoBehaviour {

    void Update()
    {
        Vector3 pos = new Vector3();
        foreach (Camera camera in Camera.allCameras)
        {
            pos += camera.transform.position;
        }
        transform.position = pos / Camera.allCameras.Length;

    }
}
