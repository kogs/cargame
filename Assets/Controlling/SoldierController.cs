﻿using UnityEngine;
using System.Collections;

public class SoldierController : MonoBehaviour {


    public bool RagDollActive { get{
        return RagDollActive;
    } 
        set{
            foreach(Rigidbody body in GetComponentsInChildren<Rigidbody>()){
                body.isKinematic = !value;
                body.detectCollisions = value;
            }
            foreach (Collider collider in GetComponentsInChildren<Collider>())
            {
                collider.enabled = value;
            }
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	 
	}
}
