﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {

    private GameObject playerPrefab;
    public Camera standardCamera;
    private GameObject[] spawnpoints;
    // Use this for initialization
    void Start()
    {
        spawnpoints = GameObject.FindGameObjectsWithTag("SpawnPoint");

        Connect();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Connect()
    {
        PhotonNetwork.ConnectUsingSettings("0.1");
    }

    void OnGUI()
    {
        GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
    }

    void OnJoinedLobby()
    {
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("OnJoinedLobby");
    }

    void OnPhotonRandomJoinFailed()
    {
        Debug.Log("OnPhotonRandomJoinFailed");
        PhotonNetwork.CreateRoom(null);
    }

    void OnJoinedRoom()
    {
        Debug.Log("OnJoinedRoom");

        SpawnMyPlayer();
    }

    void SpawnMyPlayer()
    {
        if (spawnpoints == null)
        {
            Debug.LogError("NO SPAWNS!!!!");
            return;
        }
        GameObject mySpawnpoint = spawnpoints[Random.Range(0, spawnpoints.Length)];
        GameObject myPlayerGO = (GameObject)PhotonNetwork.Instantiate("Car", mySpawnpoint.transform.position, mySpawnpoint.transform.rotation, 0);
        myPlayerGO.GetComponent<PlayerController>().playerNumber = 0;
        myPlayerGO.transform.FindChild("VisualStuff").gameObject.SetActive(true);
        standardCamera.gameObject.SetActive(false);

    }
}
