﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class CompassPanel : MonoBehaviour {

    private PlayerController player;
    public Image arrowImage;

    private Dictionary<PlayerController, Image> playerImages = new Dictionary<PlayerController, Image>();

	// Use this for initialization
	void Start () {
        player = GetComponentInParent<PlayerController>();

	}
	
	// Update is called once per frame
	void Update () {
        foreach (PlayerController aPlayer in PlayerController.Players)
        {
            if (!aPlayer.Equals(player))
            {
                 Image image;
                if (playerImages.ContainsKey(aPlayer))
                { image = playerImages[aPlayer];

                }else{
                     image = (Image)Instantiate(arrowImage,Vector3.zero,new Quaternion());
                    image.rectTransform.parent = this.transform;
                    playerImages[aPlayer] = image;
                }
                image.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Top, 0, 20);
                image.rectTransform.SetInsetAndSizeFromParentEdge(RectTransform.Edge.Left, 0, 20);

                //image.rectTransform.position = new Vector3(10,10,10);

            }
        }
	}
}
