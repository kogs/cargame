﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class MiniMapController : MonoBehaviour
{

    private Camera camera;


    void Start()
    {
        camera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        
        int players = PlayerController.Players.Count;
        switch (players)
        {
            case 1:
                {
                    camera.pixelRect = new Rect(10, 10, 200, 200);
                    break;
                }
            case 2:
                {
                    camera.pixelRect = new Rect((Screen.width - 200) / 2,0, 200, 200);
                    break;
                }
            case 3:
                {
                    camera.pixelRect = new Rect((Screen.width - 200) / 2, (Screen.height - 200) /2, 200, 200);
                    break;
                }
            case 4:
                {
                    camera.pixelRect = new Rect((Screen.width - 200) / 2, (Screen.height - 200) / 2, 200, 200);
                    break;
                }




        }


    }
}
