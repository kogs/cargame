﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TrailRenderer))]
[RequireComponent(typeof(WheelCollider))]
public class WheelGroundTrail : MonoBehaviour {

    TrailRenderer renderer;
    WheelCollider wheel;

    void Start()
    {
        renderer = GetComponent<TrailRenderer>();
        wheel = GetComponent<WheelCollider>();
    }

	// Update is called once per frame
	void Update () {
        renderer.enabled = wheel.isGrounded;
	}
}
