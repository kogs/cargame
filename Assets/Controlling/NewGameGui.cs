﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NewGameGui : MonoBehaviour {


    
    public NewGamePlayerPanel player1;
    public NewGamePlayerPanel player2;
    public NewGamePlayerPanel player3;
    public NewGamePlayerPanel player4;


    public Text countDownText;

    private bool keyBoardConnected = false;
    private bool joy1Connected = false;
    private bool joy2Connected = false;
    private bool joy3Connected = false;
    private bool joy4Connected = false;

	// Update is called once per frame
	void Update () {


        if (Input.GetKeyDown(KeyCode.Return) && !keyBoardConnected )
        {
            addPlayer(0);
            keyBoardConnected = true;
        }

        if (Input.GetKeyDown("joystick 1 button 7") && !joy1Connected)
        {
            addPlayer(1);
            joy1Connected = true;
        }
        if (Input.GetKeyDown("joystick 2 button 7") && !joy2Connected)
        {
            addPlayer(2);
            joy2Connected = true;
        }
        if (Input.GetKeyDown("joystick 3 button 7") && !joy3Connected)
        {
            addPlayer(3);
            joy3Connected = true;
        }
        if (Input.GetKeyDown("joystick 4 button 7") && !joy4Connected)
        {
            addPlayer(4);
            joy4Connected = true;
        }
        if (allReady())
        {
            if (!countDownRunning) {
                countDownRunning = true;
                StartCoroutine(CountDown());
            }
        }
        else
        {
            countDownRunning = false;
            countDown = 6;
        }


        countDownText.enabled = countDownRunning;
        countDownText.text = countDown.ToString();
	}



    private void addPlayer(int playerControllerIndex)
    {
        NewGamePlayerPanel newPlayerPanel;
        int playerSize = getActive().Count;
        switch (playerSize)
        {
            case 0:
                {
                    newPlayerPanel = player1;
                    break;
                }
            case 1:
                {
                    newPlayerPanel = player2;
                    break;
                }

            case 2:
                {
                    newPlayerPanel = player3;
                    break;
                }
            case 3:
                {
                    newPlayerPanel = player4;
                    break;
                }
            default:{
                return;
            }
        }

        newPlayerPanel.PlayerId = playerControllerIndex;
        newPlayerPanel.gameObject.SetActive(true);

    }



    private int countDown = 6;
    private bool countDownRunning = false;



    IEnumerator CountDown()
    {
        countDown--;
        if (countDown == 0 || !countDownRunning)
        {
           if (countDown == 0)
           {
               StartGame();
           }
           countDown = 6;
        }else {
          yield return new WaitForSeconds(1);
          StartCoroutine(CountDown());
        }
        
    }


    public void StartGame()
    {
        Debug.Log("Start Game");
        Application.LoadLevel("Game");
    }



    private bool allReady()
    {
        List<NewGamePlayerPanel> actives = getActive();
        if (actives.Count > 0)
        {
            foreach(NewGamePlayerPanel panel in actives){
                if (!panel.ready)
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
        return true;
    }

    private bool notActiveOrReady(NewGamePlayerPanel player){
        if (!player.gameObject.activeSelf)
        {
            return true;
        }
        return player.ready;

    }


    private List<NewGamePlayerPanel> getActive()
    {
        List<NewGamePlayerPanel> list = new List<NewGamePlayerPanel>();
        if (player1.gameObject.activeSelf)
        {
            list.Add(player1);
        }
        if (player2.gameObject.activeSelf)
        {
            list.Add(player2);
        }
        if (player3.gameObject.activeSelf)
        {
            list.Add(player3);
        }
        if (player4.gameObject.activeSelf)
        {
            list.Add(player4);
        }
        return list;
    }

}
