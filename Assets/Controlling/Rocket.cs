﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rocket : MonoBehaviour
{

    public float radius = 1000.0f;    //provides a radius at which the explosive will effect rigidbodies
    public float power = 500000.0f;    //provides explosive power
    public float explosiveLift = 1.0f; //determines how the explosion reacts. A higher value means rigidbodies will fly upward
    public Transform explosionParticle;
    public PlayerController player;

    void Start()
    {
        Destroy(gameObject,5);
    

    }

    void Update()
    {
        transform.GetChild(0).localRotation = new Quaternion(0, 0, transform.GetChild(0).localRotation.z+10,1);
    }


    void OnCollisionEnter(Collision collision)
    {

        Vector3 grenadeOrigin = transform.position;

        Collider[] colliders = Physics.OverlapSphere(grenadeOrigin, radius); //this is saying that if any collider within the radius of our object will feel the explosion

        Instantiate(explosionParticle, grenadeOrigin, transform.rotation);

        List<PlayerController> playersHit = new List<PlayerController>();
        foreach (Collider hit in colliders)
        {  //for loop that says if we hit any colliders, then do the following below

            Rigidbody rigidbody = hit.rigidbody;
            if (rigidbody == null)
            {
                rigidbody = hit.attachedRigidbody;
                
                Debug.Log(hit + " " + rigidbody);
            }

            if (rigidbody)
            {
                Debug.Log(hit);
                rigidbody.AddExplosionForce(power, grenadeOrigin, radius, explosiveLift); //if we hit any rigidbodies then add force based off our power, the position of the explosion object

                PlayerController controller = rigidbody.GetComponent<PlayerController>();
                if (controller)
                {
                    if (!playersHit.Contains(controller))
                    {
                        playersHit.Add(controller);
                        if (player != controller)
                        {
                            float dis = Vector3.Distance(grenadeOrigin,controller.transform.position);
                            int score = 100 - (int)dis;
                            if (score < 10)
                            {
                                score = 10;
                            }

                            player.score += score;
                            controller.score -= score / 10;
                        }
                        else
                        {
                            player.score -= 10;
                        }
                    }
                }
                Destroy(gameObject);    //the radius and finally the explosive lift. Afterwards destroy the game object

            }

        }

    }

}
